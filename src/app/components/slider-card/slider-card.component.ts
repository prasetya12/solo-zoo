import { Component, OnInit,ViewChild,Input } from '@angular/core';
import { SlickCarouselComponent } from "ngx-slick-carousel";

@Component({
  selector: 'app-slider-card',
  templateUrl: './slider-card.component.html',
  styleUrls: ['./slider-card.component.scss']
})
export class SliderCardComponent implements OnInit {
  @Input() place:string
  @Input() to:string


  @ViewChild('slickModal', { static: true }) slickModal: SlickCarouselComponent;
  slideConfig = {"slidesToShow": 3, 
                  "slidesToScroll": 4,
                  "responsive":[
                    {
                      "breakpoint": 1024,
                      "settings": {
                        "slidesToShow": 3
                      }
                    },
                    {
                      "breakpoint": 600,
                      "settings": {
                        "slidesToShow": 2
                      }
                    },
                    {
                      "breakpoint": 480,
                      "settings": {
                        "slidesToShow": 1
                      }
                    }
                  ]};
  constructor() { }

  ngOnInit(): void {
  }

  next() {
    this.slickModal.slickNext();
  }
  
  prev() {
    this.slickModal.slickPrev();
  }

}
