import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DestinantionCardComponent } from './destinantion-card.component';

describe('DestinantionCardComponent', () => {
  let component: DestinantionCardComponent;
  let fixture: ComponentFixture<DestinantionCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DestinantionCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DestinantionCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
