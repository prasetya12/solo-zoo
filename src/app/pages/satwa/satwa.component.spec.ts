import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SatwaComponent } from './satwa.component';

describe('SatwaComponent', () => {
  let component: SatwaComponent;
  let fixture: ComponentFixture<SatwaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SatwaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SatwaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
