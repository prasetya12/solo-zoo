import {Action,createSelector} from '@ngrx/store'

const initialState={
    cart:[]
}

export function cartReducer(state=initialState,action){
    switch(action.type){
        case "LOAD_CART":{
            return {
                ...state,
                loading:true,
                loaded:false
            }
        }

        case "ADD_CART":{
            return {
              ...state,
              cart : state.cart.concat(action.payload),
              loading:true,
              loaded:false
            }
        }
        case "DELETE_CART":{
          return {
            ...state,
            cart : state.cart.filter((item, index) => index !== action.payload),
            loading:true,
            loaded:false
          }
        }

        case "PLUS_CART":{
          const newCart = [...state.cart];
          const quantity = newCart[action.payload].quantity
          newCart[action.payload] = { ...newCart[action.payload], quantity: quantity + 1 };
          console.log(newCart,'halo')
          return {
            ...state,
            cart : newCart,
            loading:true,
            loaded:false
          }
        }

        case "MINUS_CART":{
          const newCart = [...state.cart];
          const quantity = newCart[action.payload].quantity
          newCart[action.payload] = { ...newCart[action.payload], quantity: quantity - 1 };
          console.log(newCart,'halo')
          return {
            ...state,
            cart : newCart,
            loading:true,
            loaded:false
          }
        }
        default: {
          return state;
        }
    }
}

export const selectCartState = (state) => state.cart;
export const selectCart = createSelector(selectCartState, (state) => state.cart);