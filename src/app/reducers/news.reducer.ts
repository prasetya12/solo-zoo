import { NewsActionTypes } from "../actions/news.action";

const initialState={
    isLoading:false,
    isError:false,
    dataSuccess:null,
    errorMessage:{}
}

export function newsReducer(state=initialState,action){
    switch(action.type){
        case NewsActionTypes.GET_NEWS:
            return{
                ...state,
                isLoading:true,
                isError:false
            }

        case NewsActionTypes.GET_NEWS_SUCCESS:
            return{
                ...state,
                isLoading:false,
                isError:false,
                dataSuccess:action.payload
            }
        
        case NewsActionTypes.GET_NEWS_FAILURE:
            return{
                ...state,
                isLoading:false,
                isError:true,
                errorMessage:action.payload
            }

        default:
            return state
    }
}