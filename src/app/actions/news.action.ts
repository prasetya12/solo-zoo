import {Action} from '@ngrx/store'

export enum NewsActionTypes{
    GET_NEWS = '[NEWS] GET_NEWS',
    GET_NEWS_SUCCESS = '[NEWS] GET_NEWS_SUCCESS',
    GET_NEWS_FAILURE = '[NEWS] GET_NEWS_FAILURE'
} 

export class getNews implements Action{
    readonly type = NewsActionTypes.GET_NEWS
    constructor(public payload:any=null){}
}

export class getNewsSuccess implements Action{
    readonly type = NewsActionTypes.GET_NEWS_SUCCESS
    constructor(public payload:any=null){

    }
}

export class getNewsFailure implements Action{
    readonly type = NewsActionTypes.GET_NEWS_FAILURE
    constructor(public payload:any=null){
        
    }
}