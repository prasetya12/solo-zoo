import { Injectable } from '@angular/core';
import {API} from '../../constants/urls'
import { Subject, Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders ,HttpParams } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { AngularFireDatabase} from 'angularfire2/database';
import {AngularFirestore, DocumentChangeAction} from 'angularfire2/firestore';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient, private db: AngularFireDatabase,private afs: AngularFirestore) { }

  getAttractions(params){
    // let newparams = new HttpParams().set('get_attraction', params);
    return this.http.get(API.ATTRACTIONS,{params})
  }

  getDetailAttraction(id){
    return this.http.get(API.DETAIL_ATTRACTION,{
      params:{
        id
      }
    })
  }

  getTicketType(id){
    return this.http.get(API.TICKET_TYPE,{
      params:{
        attraction_id:id
      }
    })
  }


  paginate (limit: number, last: number):  Observable<DocumentChangeAction<any>[]> {
    return this.afs.collection('products', (ref) => (
     ref
       .where('id', '<', last)
       .orderBy('id', 'desc')
       .limit(limit)
    )).snapshotChanges();
 }
}
